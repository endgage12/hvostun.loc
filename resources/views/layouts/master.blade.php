<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@lang('main.online_shop'): @yield('title')</title>

    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"></script>
    <script src="/js/bootstrap.min.js"></script>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/starter-template.css" rel="stylesheet">
</head>
<body>
<nav class="topbar">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ route('index') }}">@lang('main.online_shop')</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{ route('index') }}">@lang('main.all_products')</a></li>
                <li><a href="{{ route('categories') }}">@lang('main.categories')</a>
                </li>
                <li> <div id="search_field">
                        <input type="text" id="search-posts" placeholder="Введите для поиска...">
                        <button type="submit" class="searchButton">
                            <i class="fa fa-search"> <img src="https://img.icons8.com/pastel-glyph/30/000000/search--v2.png"/> </i>
                        </button>
                        <table>
                            <thead>
                            </thead>
                            <tbody id="dynamic-row">
                            @if(isset($posts))
                                @foreach($posts as $post)
                                    <tr>
                                        <td><a href="{{ route('sku', [$post->product->category->code, $post->product->code, $post->id]) }}"> {{ $post->product->category->code }}/{{ $post->product->code }}/{{ $post->id }}</a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div> </li>
                <li><a href="{{ route('basket') }}"><img src="https://img.icons8.com/cotton/64/000000/shopping-basket-2--v2.png" width="32px"></a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                @guest
                    <li><a href="{{ route('login') }}">@lang('main.login')</a></li>
                @endguest

                @auth
                    @admin
                    <li><a href="{{ route('home') }}">@lang('main.admin_panel')</a></li>
                @else
                    <li><a href="{{ route('person.orders.index') }}">@lang('main.my_orders')</a></li>
                    @endadmin
                    <li><a href="{{ route('get-logout') }}">@lang('main.logout')</a></li>
                @endauth
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="starter-template">
        @if(session()->has('success'))
            <p class="alert alert-success">{{ session()->get('success') }}</p>
        @endif
        @if(session()->has('warning'))
            <p class="alert alert-warning">{{ session()->get('warning') }}</p>
        @endif
        @yield('content')
    </div>
</div>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <a href="{{ route('delivery') }}">Доставка</a>
            </div>
        </div>
    </div>
</footer>

<script>
    $("#search-posts").click(function() {
        $('#dynamic-row').toggle();
    });

    $(document).on('click', function(e) {
        if (!$(e.target).closest("#search_field").length) {
            $('#dynamic-row').hide();
        }
        e.stopPropagation();
    });

    $('body').on('keyup', '#search-posts', function () {
        let searchQuest = $(this).val();
        console.log(searchQuest);

        $.ajax({
            method: 'POST',
            url: '{{ route("search-posts") }}',
            dataType: 'json',
            data: {
                '_token': '{{ csrf_token() }}',
                searchQuest: searchQuest,
            },
            success: function (res) {
                console.log(res);
                let resLength = res.length;
                let skusLength = [];
                let j = 0, i = 0;
                for(let i = 0; i < resLength; i++) {
                    skusLength[i] = res[i].skus.length;
                }


                let tableRow = '';

                $('#dynamic-row').html('');

                $.each(res, function (index, value) {
                    for(let i = 0; i < 1; i++) {
                        for(let j = 0; j < skusLength[i]; j++) {
                            tableRow = '<a href="' + value.category.code + '/' + value.code + '/' + value.skus[j].id + '"> ' +
                                '<td>' + value.name + '</td> ' + value.skus[j].id + '</a><br>'
                            ;
                            $('#dynamic-row').append(tableRow);
                        }
                    }
                });
            }
        });
    });

</script>
</body>
</html>
