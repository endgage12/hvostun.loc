@extends('layouts.master')

@section('title', __('main.product'))

@section('content')
    <h1>{{ $skus->product->__('name') }}</h1>
    <h2>{{ $skus->product->category->name }}</h2>

    @isset($skus->product->properties)
        @foreach ($skus->propertyOptions as $propertyOption)
            <h4>{{ $propertyOption->property->__('name') }}: {{ $propertyOption->__('name') }}</h4>
        @endforeach
    @endisset

    <p>@lang('product.price'): <b>{{ $skus->price }} {{ $currencySymbol }}</b></p>
    @if($skus->isAvailable())
        <form action="{{ route('basket-add', $skus) }}" method="POST">
            <button type="submit" class="btn btn-success" role="button"> {{ __('Добавить в корзину') }}</button>

            @csrf
        </form>
    @else

        <span>@lang('product.not_available')</span>
        <br>
        <span>@lang('product.tell_me'):</span>
        <div class="warning">
            @if($errors->get('email'))
                {!! $errors->get('email')[0] !!}
            @endif
        </div>
        <form method="POST" action="{{ route('subscription', $skus) }}">
            @csrf
            <input type="text" name="email"></input>
            <button type="submit">@lang('product.subscribe')</button>
        </form>
    @endif
    <img src="{{ Storage::url($skus->product->image) }}" width="240px">
    <p>{{ $skus->product->__('description') }}</p>


    <h3>Рейтинг: {{ $skus->rating }}</h3>
    <form action="{{ route('updateRate', $skus) }}" method="POST">
        @csrf
        <label for="rating">Оценить: </label>
        <select name="rating" id="rating">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
        </select>
        <input type="submit" class="btn btn-warning">
    </form>
@endsection
