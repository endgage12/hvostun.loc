@extends('auth.layouts.master')

@section('title', 'Поставщики')

@section('content')
    <div class="col-md-12">
        <h1>Список товаров</h1>
        @if(session()->has('success'))
            <p class="alert alert-success">{{ session()->get('success') }}</p>
        @endif

        <table class="table">
            <tbody>
            <tr>
                <th>
                    Название / #
                </th>
                <th>
                    Количество
                </th>

            </tr>
            @foreach($products as $product)
                <tr>
                    <td>{{$product->name}}</td>
                    @foreach($product->skus as $prod)
                        <tr>
                            <td>{{ $prod->id }}</td>
                            <td>{{ $prod->count }}</td>
                        </tr>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>

            <h1>Поставщики</h1>
        <table class="table">
            <tbody>
            <tr>
                <th>
                    #
                </th>
                <th>
                    Название
                </th>
                <th>
                    Краткое описание
                </th>
                <th>
                    Телефон
                </th>
                <th>
                    Email
                </th>
                <th>
                    Действия
                </th>
            </tr>
            @foreach($merchants as $merchant)
                <tr>
                    <td>{{ $merchant->id }}</td>
                    <td>{{ $merchant->name }}</td>
                    <td>{{ $merchant->description }}</td>
                    <td>{{ $merchant->phone }}</td>
                    <td>{{ $merchant->email }}</td>
                    <td>
                        <div class="btn-group" role="group">
                            <form action="{{ route('merchants.destroy', $merchant) }}" method="POST">
                                <a class="btn btn-success" type="button" href="{{ route('merchants.show', $merchant) }}">Открыть</a>
                                <a class="btn btn-warning" type="button" href="{{ route('merchants.edit', $merchant) }}">Редактировать</a>
{{--                                <a class="btn btn-primary" type="button" href="{{ route('merchants.update_token', $merchant) }}">Обновить токен</a>--}}
                                @csrf
                                @method('DELETE')
                                <input class="btn btn-danger" type="submit" value="Удалить"></form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $merchants->links() }}
        <a class="btn btn-success" type="button"
           href="{{ route('merchants.create') }}">Добавить поставщика</a>

        <h2>Заказанные поставки</h2>
        @foreach($merchants as $merchant)
            @if($merchant->list_order !== NULL)
                <table class="table">
                    <tbody>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            Название
                        </th>
                        <th>
                            Телефон
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Содержание
                        </th>
                        <th>
                            Дата
                        </th>
                    </tr>
                    @foreach($merchants as $merchant)
                        <tr>
                            <td>{{ $merchant->id }}</td>
                            <td>{{ $merchant->name }}</td>
                            <td>{{ $merchant->phone }}</td>
                            <td>{{ $merchant->email }}</td>
                            <td>{{ $merchant->list_order }}</td>
                            <td>{{ $merchant->date_order }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        @endforeach
    </div>
@endsection
