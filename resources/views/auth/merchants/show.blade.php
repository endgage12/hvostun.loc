@extends('auth.layouts.master')

@section('title', 'Поставщик ' . $merchant->name)

@section('content')
    <div class="col-md-12">
        <h1>Поставщик {{ $merchant->name }}</h1>
        <table class="table">
            <tbody>
            <tr>
                <th>
                    Поле
                </th>
                <th>
                    Значение
                </th>
            </tr>
            <tr>
                <td>ID</td>
                <td>{{ $merchant->id }}</td>
            </tr>
            <tr>
                <td>Название</td>
                <td>{{ $merchant->name }}</td>
            </tr>
            <tr>
                <td>Контактный номер</td>
                <td>{{ $merchant->phone }}</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>{{ $merchant->email }}</td>
            </tr>
            <tr>
                <td>Сайт</td>
                <td>{{ $merchant->site }}</td>
            </tr>
            <tr>
                <td>Краткое описание</td>
                <td>{{ $merchant->description }}</td>
            </tr>
            </tbody>
        </table>

        <form action="{{ route('sku-add', $merchant->id) }}" method="POST">
            @csrf
            <textarea name="list_order" id="list_order" cols="60" rows="10" placeholder="Список заказа"></textarea> <br>
            <input type="date" name="date_order" id="date_order">
            <input type="submit" class="btn btn-warning" value="Заказать поставку">
        </form>

        <table class="table">
            <tbody>
            <tr>
                <th>
                    Поле
                </th>
                <th>
                    Значение
                </th>
            </tr>
            <tr>
                <td>Заказанная поставка</td>
                <td>{{ $merchant->list_order }}</td>
            </tr>
            <tr>
                <td>Дата поставки</td>
                <td>{{ $merchant->date_order }}</td>
            </tr>
            </tbody>
        </table>

    </div>
@endsection
