@extends('auth.layouts.master')

@isset($merchant)
    @section('title', 'Редактировать поставщика ' . $merchant->name)
@else
    @section('title', 'Создать поставщика')
@endisset

@section('content')
    <div class="col-md-12">
        @isset($merchant)
            <h1>Редактировать поставщика <b>{{ $merchant->name }}</b></h1>
        @else
            <h1>Добавить поставщика</h1>
        @endisset

        <form method="POST" enctype="multipart/form-data"
              @isset($merchant)
              action="{{ route('merchants.update', $merchant) }}"
              @else
              action="{{ route('merchants.store') }}"
            @endisset
        >
            <div>
                @isset($merchant)
                    @method('PUT')
                @endisset
                @csrf
                <div class="input-group row">
                    <label for="name" class="col-sm-2 col-form-label">Название: </label>
                    <div class="col-sm-6">
                        @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <input type="text" class="form-control" name="name" id="name"
                               value="@isset($merchant){{ $merchant->name }}@endisset">
                    </div>
                </div>
                    <br>

                    <div class="input-group row">
                        <label for="name" class="col-sm-2 col-form-label">Телефон: </label>
                        <div class="col-sm-6">
                            @error('phone')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <input type="text" class="form-control" name="phone" id="phone"
                                   value="@isset($merchant){{ $merchant->phone }}@endisset">
                        </div>
                    </div>

                <br>
                <div class="input-group row">
                    <label for="email" class="col-sm-2 col-form-label">Email: </label>
                    <div class="col-sm-6">
                        @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <input type="text" class="form-control" name="email" id="email"
                               value="@isset($merchant){{ $merchant->email }}@endisset">
                    </div>
                </div>
                    <div class="input-group row">
                        <label for="email" class="col-sm-2 col-form-label">Краткое описание: </label>
                        <div class="col-sm-6">
                            @error('description')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <input type="text" class="form-control" name="description" id="description"
                                   value="@isset($merchant){{ $merchant->description }}@endisset">
                        </div>
                    </div>
                    <div class="input-group row">
                        <label for="email" class="col-sm-2 col-form-label">Сайт: </label>
                        <div class="col-sm-6">
                            @error('site')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <input type="text" class="form-control" name="site" id="site"
                                   value="@isset($merchant){{ $merchant->site }}@endisset">
                        </div>
                    </div>
                <button class="btn btn-success">Сохранить</button>
            </div>
        </form>
    </div>
@endsection
