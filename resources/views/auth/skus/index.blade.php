@extends('auth.layouts.master')

@section('title', 'Товарные предложения')

@section('content')
    <div class="col-md-12">
        <h1>Товарные предложения</h1>
        <h2>{{ $product->name }}</h2>
        <table class="table">
            <tbody>
            <tr>
                <th>
                    #
                </th>
                <th>
                    Товарное предложение (свойства)
                </th>
                <th>
                    Количество
                </th>
                <th>
                    Действия
                </th>
            </tr>
            @foreach($sku as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->propertyOptions->map->name->implode(', ') }}</td>
                    <td>{{ $item->count }}</td>
                    <td>
                        <div class="btn-group" role="group">
                            <form action="{{ route('skus.destroy', [$product, $item]) }}" method="POST">
                                <a class="btn btn-success" type="button" href="{{ route('skus.show', [$product, $item]) }}">Открыть</a>
                                <a class="btn btn-warning" type="button" href="{{ route('skus.edit', [$product, $item]) }}">Редактировать</a>
                                @csrf
                                @method('DELETE')
                                <input class="btn btn-danger" type="submit" value="Удалить"></form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $sku->links() }}
        <a class="btn btn-success" type="button"
           href="{{ route('skus.create', $product) }}">Добавить</a>
    </div>
@endsection
