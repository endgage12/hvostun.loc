@extends('auth.layouts.master')

@section('title', 'Заказ ' . $order->id)

@section('content')
    <div class="py-4">
        <div class="container">
            <div class="justify-content-center">
                <div class="panel">
                    <h1>Заказ №{{ $order->id }}</h1>
                    <p>Заказчик: <b>{{ $order->name }}</b></p>
                    <p>Номер телефона: <b>{{ $order->phone }}</b></p>
                    <p>Адрес доставки: {{ $order->delivery_address }}</p>
                    <p>Дата доставки: {{ $order->delivery_date }}</p>
                    <p>Время доставки: {{ $order->delivery_time }}</p>


                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th>Кол-во</th>
                            <th>Цена</th>
                            <th>Стоимость</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($sku as $item)
                            <tr>
                                <td>
                                    <a href="{{ route('sku', [$item->product->category->code, $item->product->code, $item]) }}">
                                        <img height="56px"
                                             src="{{ Storage::url($item->product->image) }}">
                                        {{ $item->product->name }}
                                    </a>
                                </td>

                                <td><span class="badge"> {{ $item->pivot->count }}</span></td>
                                <td>{{ $item->pivot->price }} {{ $order->currency->symbol }}</td>
                                <td>{{ $item->pivot->price * $item->pivot->count }} {{ $order->currency->symbol }}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3">Общая стоимость:</td>
                            <td>{{ $order->sum }} {{ $order->currency->symbol }}</td>
                        </tr>
                        @if($order->hasCoupon())
                            <tr>
                                <td colspan="3">Был использован купон:</td>
                                <td><a href="{{ route('coupons.show', $order->coupon) }}">{{ $order->coupon->code }}</a></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    <br>
                    @foreach ($sku as $item)
                    <form action="{{ route('updateRate', $item) }}" method="POST">
                        @endforeach
                        @csrf
                        <td>
                            <div class="rating-area">
                                <input type="radio" id="star-5" name="rating" value="5">
                                <label for="star-5" title="Оценка «5»"></label>
                                <input type="radio" id="star-4" name="rating" value="4">
                                <label for="star-4" title="Оценка «4»"></label>
                                <input type="radio" id="star-3" name="rating" value="3">
                                <label for="star-3" title="Оценка «3»"></label>
                                <input type="radio" id="star-2" name="rating" value="2">
                                <label for="star-2" title="Оценка «2»"></label>
                                <input type="radio" id="star-1" name="rating" value="1">
                                <label for="star-1" title="Оценка «1»"></label>
                            </div>
                        </td>
                        <input type="submit" class="btn btn-success" value="Подтвердить">
                    </form>

                </div>

                @if ($order->status_order === NULL)
                <div class="panel">
                    <h2>Оформление заказа</h2>
                    <form action="{{ route('apply_order', $order) }}" method="POST">
                        @csrf
                        <label for="delivery_address"> {{ __('Адрес доставки') }}: </label>
                        <input type="text" name="delivery_address" id="delivery_address" value="">
                        <input type="submit" class="btn btn-success" value="Подтвердить">
                    </form>

                    <h2>Удаление заказа</h2>
                    <form action="{{ route('delete_order', $order) }}" method="POST">
                        @csrf
                        <input type="submit" class="btn btn-warning" value="Удалить">
                    </form>
                </div>
                @endif

                @if ($order->status_order === "Подтвержден")
                    <h2>Завершить заказ</h2>
                    <form action="{{ route('final_confirm_order', $order) }}" method="POST">
                        @csrf
                        <input type="submit" class="btn btn-success" value="Закрыть">
                    </form>

                    <h2>Перенести заказ на другое время</h2>
                    <form action="{{ route('edit_order', $order) }}" method="POST">
                        @csrf
                        <input type="date" name="delivery_date" id="delivery_date" value="">
                            <select name="delivery_time" id="delivery_time">
                                <option disabled>Доступные промежутки</option>
                                <option value="9:00-13:00">9:00-13:00</option>
                                <option value="13:00-17:00">13:00-17:00</option>
                                <option value="17:00-21:00">17:00-21:00</option>
                            </select>
                        <input type="submit" class="btn btn-warning" value="Перенести">
                    </form>
                @endif

                @if ($order->status_order === "Закрыт")
                    <p>Заказ доставлен: {{  \Carbon\Carbon::parse($order->updated_at)->format('d-m-Y H:i')  }}</p>
                @endif


            </div>
        </div>
    </div>
@endsection
