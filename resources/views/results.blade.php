<table style="width:100%">
    <tr>
        <th>Name</th>
    </tr>
    @if (isset($results) && count($results) > 0)
        @foreach( $results as $product )
            <tr>
                <td>{{ $product->name}}</td>
            </tr>
        @endforeach
    @endif
</table>
