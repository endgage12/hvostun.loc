@extends('layouts.master')

@section('title', __('basket.place_order'))

@section('content')
    <h1>@lang('basket.approve_order'):</h1>
    <div class="container">
        <div class="row justify-content-center">
            <p>@lang('basket.full_cost'): <b>{{ $order->getFullSum() }} {{ $currencySymbol }}.</b></p>
            <form action="{{ route('basket-confirm') }}" method="POST">
                <div>
                    <p>@lang('basket.personal_data'):</p>

                    <div class="container">
                        <div class="form-group">
                            <label for="name" class="control-label col-lg-offset-3 col-lg-2">@lang('basket.data.name'): </label>
                            <div class="col-lg-4">
                                <input type="text" name="name" id="name" value="" class="form-control">
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group">
                            <label for="phone" class="control-label col-lg-offset-3 col-lg-2">@lang('basket.data.phone'): </label>
                            <div class="col-lg-4">
                                <input type="text" name="phone" id="phone" value="" class="form-control">
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group">
                            <label for="delivery_address" class="control-label col-lg-offset-3 col-lg-2"> {{ __('Адрес доставки') }}: </label>
                            <div class="col-lg-4">
                                <input type="text" name="delivery_address" id="delivery_address" value="" class="form-control">
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="form-group">
                            <label for="delivery_date" class="control-label col-lg-offset-3 col-lg-2"> {{ __('Дата доставки') }}: </label>
                            <div class="col-lg-4">
                                <input type="date" name="delivery_date" id="delivery_date" value="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="delivery_time">Время доставки</label>
                            <select name="delivery_time" id="delivery_time">
                                <option disabled>Доступные промежутки</option>
                                <option value="9:00-13:00">9:00-13:00</option>
                                <option value="13:00-17:00">13:00-17:00</option>
                                <option value="17:00-21:00">17:00-21:00</option>
                            </select>
                        </div>
                        @guest
                            <div class="form-group">
                                <label for="name" class="control-label col-lg-offset-3 col-lg-2">@lang('basket.data.email'): </label>
                                <div class="col-lg-4">
                                    <input type="text" name="email" id="email" value="" class="form-control">
                                </div>
                            </div>
                        @endguest
                    </div>
                    <br>
                    @csrf
                    <input type="submit" class="btn btn-success" value="@lang('basket.approve_order')">
                </div>
            </form>
        </div>
    </div>
@endsection
