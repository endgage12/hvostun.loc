<!DOCTYPE html>
<html>
<head>
    <title>Поиск</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        #search_field {
            border: 1px solid black;
            width: 360px;
            margin-left: 60px;
        }

        #search_field input {
            width: 90%;
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
</head>
<body>
    <div id="search_field">
        <input type="text" id="search-posts" placeholder="Введите для поиска...">
        <table>
            <thead>
            <tr> <th> Name </th> </tr>
            </thead>
            <tbody id="dynamic-row">
            @foreach($posts as $post)
                <tr>
{{--                    <td><a href="{{ route('sku',--}}
{{--                    [isset($category) ? $category->code :--}}
{{--                    $post->product->category->code, $post->product->code, $post->id]) }}"--}}
{{--                           role="button"> {{ $post->product->name }}, {{ $post->price }}, руб.</a></td>--}}
                    <td><a href="{{ route('sku', [$post->product->category->code, $post->product->code, $post->id]) }}"> {{ $post->product->category->code }}/{{ $post->product->code }}/{{ $post->id }}</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <form action="" class="search-bar">
        <input type="search" name="search" pattern=".*\S.*" required>
        <button class="search-btn" type="submit">
            <span>Search</span>
        </button>
    </form>

</body>
<script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
    <script>
        $("#search-posts").click(function() {
            $('#dynamic-row').toggle();
        });

        $(document).on('click', function(e) {
            if (!$(e.target).closest("#search_field").length) {
                $('#dynamic-row').hide();
            }
            e.stopPropagation();
        });

        $('body').on('keyup', '#search-posts', function () {
            let searchQuest = $(this).val();
            console.log(searchQuest);

            $.ajax({
                method: 'POST',
                url: '{{ route("search-posts") }}',
                dataType: 'json',
                data: {
                    '_token': '{{ csrf_token() }}',
                    searchQuest: searchQuest,
                },
                success: function (res) {
                    console.log(res);
                    let resLength = res.length;
                    let skusLength = [];
                    let j = 0, i = 0;
                    for(let i = 0; i < resLength; i++) {
                        skusLength[i] = res[i].skus.length;
                    }


                    let tableRow = '';

                    $('#dynamic-row').html('');

                    $.each(res, function (index, value) {
                        for(let i = 0; i < 1; i++) {
                            for(let j = 0; j < skusLength[i]; j++) {
                                tableRow = '<a href="' + value.category.code + '/' + value.code + '/' + value.skus[j].id + '"> ' +
                                    '<td>' + value.name + '</td> ' + value.skus[j].id + '</a><br>'
                                ;
                                $('#dynamic-row').append(tableRow);
                            }
                        }
                    });
                }
            });
        });

    </script>
</html>

