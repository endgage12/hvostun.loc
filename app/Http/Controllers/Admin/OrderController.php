<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::active()->paginate(10);
        return view('auth.orders.index', compact('orders'));
    }

    public function show(Order $order)
    {
        $sku = $order->skus()->withTrashed()->get();
        return view('auth.orders.show', compact('order', 'sku'));
    }

    public function apply(Request $request, Order $order) {
        if (isset($request->delivery_address)) {
            $order->delivery_address = $request->delivery_address;
        }
        $order->status_order = "Подтвержден";
        $order->save();
        return redirect()->route('index');
    }

    public function edit(Request $request, Order $order) {
        $order->delivery_date = $request->delivery_date;
        $order->delivery_time = $request->delivery_time;
        $order->save();
        return redirect()->route('index');
    }

    public function delete(Order $order) {
        $order->delete();
        return redirect()->route('index');
    }

    public function finalConfirm(Order $order) {
        $order->status_order = "Закрыт";
        $order->save();
        return redirect()->route('index');
    }
}
