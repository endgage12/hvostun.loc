<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductsFilterRequest;
use App\Models\Product;
use App\Models\Sku;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(ProductsFilterRequest $request) {
        $postsQuery = Sku::with(['product', 'product.category'])->whereHas('product', function ($query) {
            $query->where('name', 'like', '%%');
        });

        $posts = $postsQuery->paginate(42)->withPath("?".$request->getQueryString());

        return view('test_search', compact('posts'));
    }

    public function searchPostsTemplate(Request $request) {
         $posts = Product::where('name', 'like' , '%' . $request->get('searchQuest') . '%')->get();

         return json_encode($posts);
    }

    public function searchPosts(Request $request) {

        $posts = Product::where('name', 'like' , '%' . $request->get('searchQuest') . '%')->with('skus', 'category')->get();

        return json_encode($posts);
    }
}
