-- Adminer 4.7.6 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `image` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name_en` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `categories` (`id`, `name`, `code`, `description`, `image`, `created_at`, `updated_at`, `name_en`, `description_en`) VALUES
(1,	'Корм для птиц',	'birds_food',	'Корм для птиц',	'categories/mobile.jpg',	'2021-03-09 07:56:01',	'2021-03-10 09:42:23',	'Корм для птиц',	'Корм для птиц'),
(2,	'Корм для грызунов',	'gryzuny',	'Корм для грызунов',	'categories/portable.jpg',	'2021-03-09 07:56:02',	'2021-03-10 09:43:29',	'Корм для грызунов',	'Корм для грызунов'),
(3,	'Корм для кошек',	'cats_food',	'Корм для кошек',	'categories/appliance.jpg',	'2021-03-09 07:56:02',	'2021-03-10 09:43:50',	'Корм для кошек',	'Корм для кошек'),
(4,	'Корм для собак',	'dogs_food',	'Корм для собак',	NULL,	'2021-03-10 09:18:01',	'2021-03-10 09:18:01',	'Корм для собак',	'Корм для собак');

DROP TABLE IF EXISTS `coupons`;
CREATE TABLE `coupons` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` double NOT NULL,
  `type` tinyint unsigned NOT NULL DEFAULT '0',
  `currency_id` int unsigned DEFAULT NULL,
  `only_once` tinyint unsigned NOT NULL DEFAULT '0',
  `expired_at` timestamp NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `currencies`;
CREATE TABLE `currencies` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_main` tinyint NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `currencies` (`id`, `code`, `symbol`, `is_main`, `rate`, `created_at`, `updated_at`) VALUES
(1,	'RUB',	'₽',	1,	1,	'2021-03-09 07:56:01',	'2021-03-09 07:56:01'),
(2,	'USD',	'$',	0,	0,	'2021-03-09 07:56:01',	'2021-03-09 07:56:01'),
(3,	'EUR',	'€',	0,	0,	'2021-03-09 07:56:01',	'2021-03-09 07:56:01');

DROP TABLE IF EXISTS `merchants`;
CREATE TABLE `merchants` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(21) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `merchants` (`id`, `name`, `email`, `phone`, `token`, `created_at`, `updated_at`) VALUES
(1,	'Семаков И.С.',	'semakov@mail.com',	'798651',	NULL,	'2021-03-10 02:55:42',	'2021-03-10 10:07:26');

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(8,	'2021_02_25_075749_create_orders_table',	2),
(9,	'2021_02_25_080102_create_order_product_table',	3),
(106,	'2014_10_12_100000_create_password_resets_table',	4),
(107,	'2021_02_25_052630_create_products_table',	4),
(108,	'2021_02_25_053308_create_categories_table',	4),
(109,	'2021_02_25_104718_create_orders_table',	4),
(110,	'2021_02_25_105118_create_order_product_table',	4),
(111,	'2021_02_26_042716_update_order_product_table',	4),
(112,	'2021_02_26_064329_create_users_table',	4),
(113,	'2021_02_26_094022_alter_table_users',	4),
(114,	'2021_02_26_120417_alter_table_orders',	4),
(115,	'2021_02_27_112046_alter_table_products',	4),
(116,	'2021_02_28_064029_alter_table_products_count',	4),
(117,	'2021_02_28_072308_create_subscriptions_table',	4),
(118,	'2021_02_28_075138_localization_products',	4),
(119,	'2021_02_28_075211_localization_categories',	4),
(120,	'2021_03_02_062203_create_currencies_table',	4),
(121,	'2021_03_02_064605_alter_table_orders_add_currency_id_and_sum',	4),
(122,	'2021_03_02_064643_alter_order_product_add_price',	4),
(123,	'2021_03_02_072115_create_skus_table',	4),
(124,	'2021_03_02_072153_create_properties_table',	4),
(125,	'2021_03_02_072231_create_property_options_table',	4),
(126,	'2021_03_02_072303_create_sku_property_option_table',	4),
(127,	'2021_03_02_072330_create_property_product',	4),
(128,	'2021_03_05_071021_alter_table_products_drop_columns_count_and_price',	4),
(129,	'2021_03_05_072247_alter_subscription_table',	4),
(130,	'2021_03_05_072339_order_sku',	4),
(131,	'2021_03_05_072413_alter_skus_soft_deletes',	4),
(132,	'2021_03_05_081112_alter_orders_table',	4),
(133,	'2021_03_05_081157_create_coupons_table',	4),
(134,	'2021_03_05_083416_create_merchants_table',	4);

DROP TABLE IF EXISTS `order_sku`;
CREATE TABLE `order_sku` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int NOT NULL,
  `sku_id` int NOT NULL,
  `count` int NOT NULL,
  `price` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint NOT NULL DEFAULT '0',
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `currency_id` int NOT NULL,
  `sum` double NOT NULL,
  `coupon_id` int unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `image` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `new` tinyint NOT NULL DEFAULT '0',
  `hit` tinyint NOT NULL DEFAULT '0',
  `recommend` tinyint NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name_en` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `products` (`id`, `category_id`, `name`, `code`, `description`, `image`, `created_at`, `updated_at`, `new`, `hit`, `recommend`, `deleted_at`, `name_en`, `description_en`) VALUES
(1,	4,	'Royal Canin для собак Йоркширский терьер паштет',	'royal_canin_for_dogs_yorsh',	'Йоркширский терьер — это сказочное создание, которое очаровывает с первой минуты знакомства с ним. Тоненькие, хрупкие и изящные йорки в силу своей физиологии нуждаются в максимальном поступлении в организм аминокислот, необходимых для развития шерсти и ее быстрого роста.\r\nСостав:Мясо (свинина, курица), целлюлоза, кукурузный крахмал, жом сахарной свеклы, рыбий жир, сахара, натуральный ароматизатор, экстракт бархатцев прямостоячих (источник лютеина), - аминокислоты (таурин, метионин), стабилизатор (полисахарид загуститель) - цеолиты - минералы (цинк, железо, марганец, натрий, фосфор, калий, хлориды, йод, медь, кальций, цинк в хелатной форме, марганец, магний в хелатной форме, медь в хелатной форме) - витамины (ниацин, холин, D3, E, C, пантотенат кальция, B1, B2, B6, фолиевая кислота, биотин, B12).',	'products/jovNUgBYq59zPoxq14Mrw6lb34gAUt26za80OtNB.jpg',	'2021-03-09 07:56:01',	'2021-03-10 09:48:14',	1,	0,	0,	NULL,	'Royal Canin для собак Йоркширский терьер паштет',	'Йоркширский терьер — это сказочное создание, которое очаровывает с первой минуты знакомства с ним. Тоненькие, хрупкие и изящные йорки в силу своей физиологии нуждаются в максимальном поступлении в организм аминокислот, необходимых для развития шерсти и ее быстрого роста.\r\nСостав:Мясо (свинина, курица), целлюлоза, кукурузный крахмал, жом сахарной свеклы, рыбий жир, сахара, натуральный ароматизатор, экстракт бархатцев прямостоячих (источник лютеина), - аминокислоты (таурин, метионин), стабилизатор (полисахарид загуститель) - цеолиты - минералы (цинк, железо, марганец, натрий, фосфор, калий, хлориды, йод, медь, кальций, цинк в хелатной форме, марганец, магний в хелатной форме, медь в хелатной форме) - витамины (ниацин, холин, D3, E, C, пантотенат кальция, B1, B2, B6, фолиевая кислота, биотин, B12).'),
(2,	1,	'iPhone XL',	'iphone_xl',	'Огромный продвинутый телефон',	'products/iphone_x_silver.jpg',	'2021-03-09 07:56:02',	'2021-03-09 10:32:13',	0,	0,	1,	'2021-03-09 10:32:13',	'iPhone XL',	'The best huge phone'),
(3,	1,	'HTC One S',	'htc_one_s',	'Зачем платить за лишнее? Легендарный HTC One S',	'products/htc_one_s.png',	'2021-03-09 07:56:02',	'2021-03-09 09:57:07',	0,	0,	0,	'2021-03-09 09:57:07',	'HTC One S',	'Why do you need to pay more? Legendary HTC One S'),
(4,	1,	'iPhone 5SE',	'iphone_5se',	'Отличный классический iPhone',	'products/iphone_5.jpg',	'2021-03-09 07:56:02',	'2021-03-09 09:57:08',	0,	1,	1,	'2021-03-09 09:57:08',	'iPhone 5SE',	'The best classic iPhone'),
(5,	1,	'Samsung Galaxy J6',	'samsung_j6',	'Современный телефон начального уровня',	'products/samsung_j6.jpg',	'2021-03-09 07:56:02',	'2021-03-09 09:57:10',	0,	0,	0,	'2021-03-09 09:57:10',	'Samsung Galaxy J6',	'Modern phone of basic level'),
(6,	2,	'Наушники Beats Audio',	'beats_audio',	'Отличный звук от Dr. Dre',	'products/beats.jpg',	'2021-03-09 07:56:02',	'2021-03-09 09:57:11',	0,	1,	0,	'2021-03-09 09:57:11',	'Headphones Beats Audio',	'Great sound from Dr. Dre'),
(7,	2,	'Камера GoPro',	'gopro',	'Снимай самые яркие моменты с помощью этой камеры',	'products/gopro.jpg',	'2021-03-09 07:56:02',	'2021-03-09 09:57:12',	0,	1,	0,	'2021-03-09 09:57:12',	'Camera GoPro',	'Capture the best moments of your life with that camera'),
(8,	2,	'Камера Panasonic HC-V770',	'panasonic_hc-v770',	'Для серьёзной видео съемки нужна серьёзная камера. Panasonic HC-V770 для этих целей лучший выбор!',	'products/video_panasonic.jpg',	'2021-03-09 07:56:02',	'2021-03-09 09:57:14',	0,	1,	0,	'2021-03-09 09:57:14',	'Camera Panasonic HC-V770',	'For serious video you need the profession camera. Panasonic HC-V770 is that you need!'),
(9,	3,	'Кофемашина DeLongi',	'delongi',	'Хорошее утро начинается с хорошего кофе!',	'products/delongi.jpg',	'2021-03-09 07:56:02',	'2021-03-09 10:07:51',	0,	0,	0,	'2021-03-09 10:07:51',	'Coffee machine DeLongi',	'Good morning starts with a good coffee!'),
(10,	3,	'Холодильник Haier',	'haier',	'Для большой семьи большой холодильник!',	'products/haier.jpg',	'2021-03-09 07:56:02',	'2021-03-09 09:57:18',	0,	0,	1,	'2021-03-09 09:57:18',	'Refrigerator Haier',	'The huge refrigerator for a big family!'),
(11,	3,	'Блендер Moulinex',	'moulinex',	'Для самых смелых идей',	'products/moulinex.jpg',	'2021-03-09 07:56:02',	'2021-03-09 09:57:17',	0,	0,	1,	'2021-03-09 09:57:17',	'Blender Moulinex',	'For best ideas'),
(12,	3,	'Мясорубка Bosch',	'bosch',	'Любите домашние котлеты? Вам определенно стоит посмотреть на эту мясорубку!',	'products/bosch.jpg',	'2021-03-09 07:56:02',	'2021-03-09 09:57:15',	0,	1,	0,	'2021-03-09 09:57:15',	'Food processor Bosch',	'Do you like home cutlets? You need to see that combine!'),
(13,	3,	'test1',	'test1',	'test1test1test1test1test1test1test1test1test1',	NULL,	'2021-03-09 10:08:18',	'2021-03-09 10:09:59',	0,	1,	0,	'2021-03-09 10:09:59',	'test1',	'test1test1test1test1test1test1test1'),
(14,	3,	'test2',	'test2',	'test2test2test2test2test2',	NULL,	'2021-03-09 10:09:06',	'2021-03-09 10:31:56',	0,	0,	1,	'2021-03-09 10:31:56',	'test2',	'test2test2test2test2test2'),
(15,	2,	'test3',	'test3',	'test3test3test3test3',	NULL,	'2021-03-09 10:10:25',	'2021-03-09 10:31:55',	0,	0,	0,	'2021-03-09 10:31:55',	'test3',	'test3test3test3test3'),
(16,	1,	'test4',	'test4',	'test4test4test4',	NULL,	'2021-03-09 10:11:56',	'2021-03-09 10:31:53',	0,	0,	0,	'2021-03-09 10:31:53',	'test4',	'test4test4test4test4'),
(17,	4,	'Royal Canin для щенков Mini Puppy в соусе',	'royal_canin_for_puppy_mini_85',	'Состав: мясо и мясные субпродукты, злаки, субпродукты растительного происхождения, масла и жиры, минеральные вещества, дрожжи, источники углеводов.',	'products/WJsWRgw7P8cDh1yysy0Pk2DUIvM3Qn5JpKRWutZL.png',	'2021-03-09 10:41:23',	'2021-03-10 09:48:28',	1,	0,	0,	NULL,	'Royal Canin для щенков Mini Puppy в соусе',	'Состав: мясо и мясные субпродукты, злаки, субпродукты растительного происхождения, масла и жиры, минеральные вещества, дрожжи, источники углеводов.'),
(18,	4,	'Royal Canin для собак Medium Adult в соусе',	'royal_canin_for_dogs_medium_adult_140',	'Полнорационный корм для взрослых собак средних размеров (весом от 10 до 25 кг) в возрасте от 12 месяцев до 10 лет\r\nСостав: мясо и мясные субпродукты, масла и жиры, злаки, субпродукты растительного происхождения, экстракты белков растительного происхождения, минеральные вещества, дрожжи, источники углеводов.',	'products/ovPZmH5r34t1qbnXSApeosBBCwIhuB5cCDgdd3tg.jpg',	'2021-03-10 09:39:26',	'2021-03-10 09:48:38',	1,	0,	0,	NULL,	'Royal Canin для собак Medium Adult в соусе',	'Полнорационный корм для взрослых собак средних размеров (весом от 10 до 25 кг) в возрасте от 12 месяцев до 10 лет\r\nСостав: мясо и мясные субпродукты, масла и жиры, злаки, субпродукты растительного происхождения, экстракты белков растительного происхождения, минеральные вещества, дрожжи, источники углеводов.'),
(19,	4,	'Royal Canin для щенков Urban Life соус',	'royal_canin_for_puppy_urban_life_150',	'Полнорационный корм для щенков (вес взрослой собаки менее 44 кг) в возрасте до 10/15 месяцев, живущих в городских условиях. URBAN LIFE JUNIOR содержит уникальный комплекс антиоксидантов, помогающий собакам, живущим в городе, справляться с оксидативным стрессом, вызванным загрязнением окружающей среды. Продукт содержит ценные питательные вещества из рыбного сырья, которые также способствуют поддержанию здоровья городской собаки, часто сталкивающейся со стрессовыми ситуациями (городской шум, скопления людей,автомобильное движение).\r\nСостав: Мясо и другие продукты животного происхождения, злаки, масла и жиры, минеральные вещества, экстракты растительных белков, продукты растительного происхождения, рыба и рыбопродукты, сахара, дрожжи.',	'products/dP0PTyhOdVDBsZFNgg9orRNndP7DzsFVl0HfCLOW.png',	'2021-03-10 09:54:30',	'2021-03-10 09:54:30',	1,	0,	0,	NULL,	'Royal Canin для щенков Urban Life соус',	'Полнорационный корм для щенков (вес взрослой собаки менее 44 кг) в возрасте до 10/15 месяцев, живущих в городских условиях. URBAN LIFE JUNIOR содержит уникальный комплекс антиоксидантов, помогающий собакам, живущим в городе, справляться с оксидативным стрессом, вызванным загрязнением окружающей среды. Продукт содержит ценные питательные вещества из рыбного сырья, которые также способствуют поддержанию здоровья городской собаки, часто сталкивающейся со стрессовыми ситуациями (городской шум, скопления людей,автомобильное движение).\r\nСостав: Мясо и другие продукты животного происхождения, злаки, масла и жиры, минеральные вещества, экстракты растительных белков, продукты растительного происхождения, рыба и рыбопродукты, сахара, дрожжи.'),
(20,	3,	'Pro Plan Sterilised лосось/тунец',	'pro_plan_sterilised_losos_tunec',	'Про План для кошек кастрированных и стерилизованных\r\nДля поддержания здоровья стерилизованных кошек. Сочетает все основные питательные вещества, включая витамины A, C и E и жирные кислоты омега-3 и омега-6, в богатой белками, нежирной рецептуре, которая также помогает обеспечить сбалансированный уровень pH мочи.\r\nСодержит OPTIRENAL®, уникальную смесь для поддержания здоровья почек. OPTIRENAL® – это научно разработанное сочетание ингредиентов, в том числе аминокислот, незаменимых жирных кислот и антиоксидантов.\r\nИнгредиенты: Лососина (20 %), кукурузная мука, рис, обезвоженный белок птицы, кукуруза, обезвоженный белок тунца, пшеничная клетчатка, концентрат горохового белка, пшеничная клейковина, яичный порошок, минералы, животный жир, гидролизат, дрожжи.',	'products/6nWdgWL6YZ2pSGD0R0Xl0ZunK05FtjG50v03PxzP.jpg',	'2021-03-10 09:58:06',	'2021-03-10 09:58:06',	1,	1,	0,	NULL,	'Pro Plan Sterilised лосось/тунец',	'Про План для кошек кастрированных и стерилизованных\r\nДля поддержания здоровья стерилизованных кошек. Сочетает все основные питательные вещества, включая витамины A, C и E и жирные кислоты омега-3 и омега-6, в богатой белками, нежирной рецептуре, которая также помогает обеспечить сбалансированный уровень pH мочи.\r\nСодержит OPTIRENAL®, уникальную смесь для поддержания здоровья почек. OPTIRENAL® – это научно разработанное сочетание ингредиентов, в том числе аминокислот, незаменимых жирных кислот и антиоксидантов.\r\nИнгредиенты: Лососина (20 %), кукурузная мука, рис, обезвоженный белок птицы, кукуруза, обезвоженный белок тунца, пшеничная клетчатка, концентрат горохового белка, пшеничная клейковина, яичный порошок, минералы, животный жир, гидролизат, дрожжи.');

DROP TABLE IF EXISTS `properties`;
CREATE TABLE `properties` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `properties` (`id`, `name`, `name_en`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	'Цвет',	'Color',	'2021-03-09 07:56:01',	'2021-03-10 09:52:09',	'2021-03-10 09:52:09'),
(2,	'Внутренняя память',	'Memory',	'2021-03-09 07:56:01',	'2021-03-10 09:52:10',	'2021-03-10 09:52:10'),
(3,	'Вес',	'Weight',	'2021-03-10 09:28:49',	'2021-03-10 09:28:49',	NULL);

DROP TABLE IF EXISTS `property_options`;
CREATE TABLE `property_options` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `property_id` int unsigned NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `property_options` (`id`, `property_id`, `name`, `name_en`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	'Белый',	'White',	'2021-03-09 07:56:01',	'2021-03-10 09:29:02',	'2021-03-10 09:29:02'),
(2,	1,	'Черный',	'Black',	'2021-03-09 07:56:01',	'2021-03-10 09:29:03',	'2021-03-10 09:29:03'),
(3,	1,	'Серебристый',	'Silver',	'2021-03-09 07:56:01',	'2021-03-10 09:29:04',	'2021-03-10 09:29:04'),
(4,	1,	'Золотой',	'Gold',	'2021-03-09 07:56:01',	'2021-03-10 09:29:06',	'2021-03-10 09:29:06'),
(5,	1,	'Красный',	'Red',	'2021-03-09 07:56:01',	'2021-03-10 09:29:08',	'2021-03-10 09:29:08'),
(6,	1,	'Синий',	'Blue',	'2021-03-09 07:56:01',	'2021-03-10 09:29:09',	'2021-03-10 09:29:09'),
(7,	2,	'32гб',	'32gb',	'2021-03-09 07:56:01',	'2021-03-10 09:29:10',	'2021-03-10 09:29:10'),
(8,	2,	'64гб',	'64gb',	'2021-03-09 07:56:01',	'2021-03-10 09:29:12',	'2021-03-10 09:29:12'),
(9,	2,	'128гб',	'128gb',	'2021-03-09 07:56:01',	'2021-03-10 09:29:14',	'2021-03-10 09:29:14'),
(10,	3,	'85 гр.',	'85 gr.',	'2021-03-10 09:29:29',	'2021-03-10 09:37:56',	NULL),
(11,	3,	'140 гр.',	'140 gr.',	'2021-03-10 09:37:50',	'2021-03-10 09:37:50',	NULL),
(12,	3,	'150 гр.',	'150 гр.',	'2021-03-10 09:55:45',	'2021-03-10 09:55:45',	NULL),
(13,	3,	'200 гр.',	'200 гр.',	'2021-03-10 09:58:38',	'2021-03-10 09:58:38',	NULL);

DROP TABLE IF EXISTS `property_product`;
CREATE TABLE `property_product` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int unsigned NOT NULL,
  `property_id` int unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `property_product` (`id`, `product_id`, `property_id`, `created_at`, `updated_at`) VALUES
(3,	2,	1,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(4,	2,	2,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(5,	3,	1,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(6,	3,	2,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(7,	4,	1,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(8,	4,	2,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(9,	5,	1,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(10,	5,	2,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(11,	6,	1,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(12,	9,	1,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(13,	10,	1,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(14,	14,	1,	'2021-03-09 10:09:33',	'2021-03-09 10:09:33'),
(15,	15,	1,	'2021-03-09 10:10:39',	'2021-03-09 10:10:39'),
(16,	16,	2,	'2021-03-09 10:12:08',	'2021-03-09 10:12:08'),
(18,	1,	3,	'2021-03-10 09:30:06',	'2021-03-10 09:30:06'),
(19,	17,	3,	'2021-03-10 09:32:47',	'2021-03-10 09:32:47'),
(20,	18,	3,	'2021-03-10 09:39:40',	'2021-03-10 09:39:40'),
(21,	19,	3,	'2021-03-10 09:55:01',	'2021-03-10 09:55:01'),
(22,	20,	3,	'2021-03-10 09:58:18',	'2021-03-10 09:58:18');

DROP TABLE IF EXISTS `sku_property_option`;
CREATE TABLE `sku_property_option` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `property_option_id` int unsigned NOT NULL,
  `sku_id` int unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `sku_property_option` (`id`, `property_option_id`, `sku_id`, `created_at`, `updated_at`) VALUES
(1,	1,	1,	'2021-03-09 07:56:01',	'2021-03-09 07:56:01'),
(2,	7,	1,	'2021-03-09 07:56:01',	'2021-03-09 07:56:01'),
(3,	1,	2,	'2021-03-09 07:56:01',	'2021-03-09 07:56:01'),
(4,	8,	2,	'2021-03-09 07:56:01',	'2021-03-09 07:56:01'),
(5,	2,	3,	'2021-03-09 07:56:01',	'2021-03-09 07:56:01'),
(6,	7,	3,	'2021-03-09 07:56:01',	'2021-03-09 07:56:01'),
(7,	2,	4,	'2021-03-09 07:56:01',	'2021-03-09 07:56:01'),
(8,	8,	4,	'2021-03-09 07:56:01',	'2021-03-09 07:56:01'),
(9,	3,	5,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(10,	7,	5,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(11,	3,	6,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(12,	8,	6,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(13,	4,	7,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(14,	7,	7,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(15,	4,	8,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(16,	8,	8,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(17,	1,	9,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(18,	8,	9,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(19,	1,	10,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(20,	9,	10,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(21,	2,	11,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(22,	8,	11,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(23,	2,	12,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(24,	9,	12,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(25,	3,	13,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(26,	8,	13,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(27,	3,	14,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(28,	9,	14,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(29,	4,	15,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(30,	8,	15,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(31,	4,	16,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(32,	9,	16,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(33,	2,	17,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(34,	7,	17,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(35,	2,	18,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(36,	8,	18,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(37,	1,	19,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(38,	7,	19,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(39,	1,	20,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(40,	8,	20,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(41,	3,	21,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(42,	7,	21,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(43,	3,	22,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(44,	8,	22,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(45,	4,	23,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(46,	7,	23,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(47,	4,	24,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(48,	8,	24,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(49,	4,	25,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(50,	7,	25,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(51,	2,	26,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(52,	5,	27,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(53,	6,	28,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(54,	2,	31,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(55,	5,	32,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(56,	6,	33,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(57,	1,	34,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(58,	2,	35,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(59,	3,	36,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02'),
(60,	4,	39,	'2021-03-09 09:42:43',	'2021-03-09 09:42:43'),
(61,	9,	39,	'2021-03-09 09:42:43',	'2021-03-09 09:42:43'),
(62,	1,	40,	'2021-03-09 09:53:57',	'2021-03-09 09:53:57'),
(63,	7,	40,	'2021-03-09 09:53:57',	'2021-03-09 09:53:57'),
(64,	1,	41,	'2021-03-09 10:06:59',	'2021-03-09 10:06:59'),
(65,	7,	41,	'2021-03-09 10:06:59',	'2021-03-09 10:06:59'),
(66,	5,	44,	'2021-03-09 10:09:46',	'2021-03-09 10:09:46'),
(67,	2,	45,	'2021-03-09 10:10:51',	'2021-03-09 10:10:51'),
(68,	1,	46,	'2021-03-09 10:11:01',	'2021-03-09 10:11:01'),
(69,	3,	47,	'2021-03-09 10:11:09',	'2021-03-09 10:11:09'),
(70,	7,	48,	'2021-03-09 10:12:16',	'2021-03-09 10:12:16'),
(71,	8,	49,	'2021-03-09 10:12:26',	'2021-03-09 10:12:26'),
(72,	9,	50,	'2021-03-09 10:12:32',	'2021-03-09 10:12:32'),
(73,	1,	52,	'2021-03-09 10:43:38',	'2021-03-09 10:43:38'),
(74,	10,	53,	'2021-03-10 09:30:21',	'2021-03-10 09:30:21'),
(75,	10,	54,	'2021-03-10 09:33:32',	'2021-03-10 09:33:32'),
(76,	11,	55,	'2021-03-10 09:39:56',	'2021-03-10 09:39:56'),
(77,	11,	56,	'2021-03-10 09:55:28',	'2021-03-10 09:55:28'),
(78,	10,	57,	'2021-03-10 09:56:07',	'2021-03-10 09:56:07'),
(79,	12,	58,	'2021-03-10 09:56:22',	'2021-03-10 09:56:22'),
(80,	13,	59,	'2021-03-10 09:59:03',	'2021-03-10 09:59:03');

DROP TABLE IF EXISTS `skus`;
CREATE TABLE `skus` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int unsigned NOT NULL,
  `count` int unsigned NOT NULL DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `skus` (`id`, `product_id`, `count`, `price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	61,	8385,	'2021-03-09 07:56:01',	'2021-03-09 09:50:49',	'2021-03-09 09:50:49'),
(2,	1,	19,	44530,	'2021-03-09 07:56:01',	'2021-03-09 09:54:03',	'2021-03-09 09:54:03'),
(3,	1,	35,	62095,	'2021-03-09 07:56:01',	'2021-03-09 09:54:04',	'2021-03-09 09:54:04'),
(4,	1,	55,	13969,	'2021-03-09 07:56:01',	'2021-03-09 09:54:06',	'2021-03-09 09:54:06'),
(5,	1,	43,	52638,	'2021-03-09 07:56:01',	'2021-03-09 09:54:07',	'2021-03-09 09:54:07'),
(6,	1,	83,	99047,	'2021-03-09 07:56:02',	'2021-03-09 09:54:09',	'2021-03-09 09:54:09'),
(7,	1,	41,	26071,	'2021-03-09 07:56:02',	'2021-03-09 09:54:10',	'2021-03-09 09:54:10'),
(8,	1,	89,	59222,	'2021-03-09 07:56:02',	'2021-03-10 09:26:03',	'2021-03-10 09:26:03'),
(9,	2,	91,	62271,	'2021-03-09 07:56:02',	'2021-03-09 10:06:46',	'2021-03-09 10:06:46'),
(10,	2,	95,	89087,	'2021-03-09 07:56:02',	'2021-03-09 10:03:23',	'2021-03-09 10:03:23'),
(11,	2,	42,	26563,	'2021-03-09 07:56:02',	'2021-03-09 10:03:20',	'2021-03-09 10:03:20'),
(12,	2,	8,	45990,	'2021-03-09 07:56:02',	'2021-03-09 10:03:20',	'2021-03-09 10:03:20'),
(13,	2,	79,	44280,	'2021-03-09 07:56:02',	'2021-03-09 10:03:20',	'2021-03-09 10:03:20'),
(14,	2,	14,	5735,	'2021-03-09 07:56:02',	'2021-03-09 10:03:19',	'2021-03-09 10:03:19'),
(15,	2,	5,	19833,	'2021-03-09 07:56:02',	'2021-03-09 10:03:19',	'2021-03-09 10:03:19'),
(16,	2,	100,	99685,	'2021-03-09 07:56:02',	'2021-03-09 10:03:17',	'2021-03-09 10:03:17'),
(17,	3,	24,	8061,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:05'),
(18,	3,	13,	19681,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(19,	4,	96,	54360,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(20,	4,	1,	82082,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(21,	4,	7,	92183,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(22,	4,	3,	5963,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(23,	4,	57,	49178,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(24,	4,	49,	47596,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(25,	5,	4,	60456,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(26,	6,	77,	37640,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(27,	6,	63,	50143,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(28,	6,	28,	9868,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(29,	7,	60,	60233,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(30,	8,	70,	12912,	'2021-03-09 07:56:02',	'2021-03-09 08:44:31',	'2021-03-09 17:38:51'),
(31,	9,	83,	14708,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(32,	9,	93,	25036,	'2021-03-09 07:56:02',	'2021-03-09 08:34:01',	'2021-03-09 17:38:51'),
(33,	9,	33,	32297,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(34,	10,	5,	9228,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(35,	10,	57,	63981,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(36,	10,	23,	98363,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(37,	11,	5,	61532,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(38,	12,	22,	20170,	'2021-03-09 07:56:02',	'2021-03-09 07:56:02',	'2021-03-09 17:38:51'),
(39,	1,	6,	120000,	'2021-03-09 09:42:43',	'2021-03-09 09:43:38',	'2021-03-09 17:38:51'),
(40,	1,	3,	50000,	'2021-03-09 09:53:57',	'2021-03-09 09:53:57',	'2021-03-09 17:38:51'),
(41,	2,	5,	60000,	'2021-03-09 10:06:59',	'2021-03-09 10:06:59',	'2021-03-09 17:38:51'),
(42,	13,	1,	123123,	'2021-03-09 10:08:29',	'2021-03-09 10:08:29',	'2021-03-09 17:38:51'),
(43,	14,	5,	75474,	'2021-03-09 10:09:17',	'2021-03-09 10:09:49',	'2021-03-09 17:38:51'),
(44,	14,	125125,	134124,	'2021-03-09 10:09:46',	'2021-03-09 10:09:46',	'2021-03-09 17:38:51'),
(45,	15,	125,	62326,	'2021-03-09 10:10:51',	'2021-03-09 10:10:51',	'2021-03-09 17:38:51'),
(46,	15,	512,	32234,	'2021-03-09 10:11:01',	'2021-03-09 10:11:01',	'2021-03-09 17:38:51'),
(47,	15,	113,	125162,	'2021-03-09 10:11:09',	'2021-03-09 10:11:09',	'2021-03-09 17:38:51'),
(48,	16,	12,	123125,	'2021-03-09 10:12:16',	'2021-03-09 10:12:16',	'2021-03-09 17:38:51'),
(49,	16,	24,	125125,	'2021-03-09 10:12:26',	'2021-03-09 10:12:26',	'2021-03-09 17:38:51'),
(50,	16,	223,	125125,	'2021-03-09 10:12:32',	'2021-03-09 10:12:32',	'2021-03-09 17:42:47'),
(51,	17,	5,	123123,	'2021-03-09 10:41:30',	'2021-03-09 10:41:55',	'2021-03-09 17:44:22'),
(52,	17,	3,	12345,	'2021-03-09 10:43:38',	'2021-03-10 09:33:35',	'2021-03-10 09:33:35'),
(53,	1,	50,	79,	'2021-03-10 09:30:21',	'2021-03-10 09:30:21',	NULL),
(54,	17,	50,	66,	'2021-03-10 09:33:32',	'2021-03-10 09:33:32',	NULL),
(55,	18,	50,	85,	'2021-03-10 09:39:56',	'2021-03-10 09:39:56',	NULL),
(56,	19,	50,	98,	'2021-03-10 09:55:28',	'2021-03-10 09:55:28',	NULL),
(57,	19,	50,	98,	'2021-03-10 09:56:07',	'2021-03-10 09:56:07',	NULL),
(58,	19,	50,	98,	'2021-03-10 09:56:22',	'2021-03-10 09:56:22',	NULL),
(59,	20,	50,	133,	'2021-03-10 09:59:03',	'2021-03-10 09:59:03',	NULL);

DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE `subscriptions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT '0',
  `sku_id` int unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_admin` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`, `is_admin`) VALUES
(1,	'Администратор',	'admin@example.com',	'$2y$10$5W9gYH7LiVoTRXhpcbkQsOS0wRizn2NaUSNoy5HgXPfdKHJQWx3xe',	NULL,	NULL,	1),
(2,	'admin',	'admin@admin.com',	'$2y$10$ib/itFuxYh7RDrtTcpQsCu6S5OQdQw4YgTJoCROchb38xm10dwWSy',	'2021-03-09 07:57:36',	'2021-03-09 07:57:36',	1),
(3,	'User',	'user@user.com',	'$2y$10$j70YWYg7tqvUDQ6Da2e7buLiRI7UzDnenVcRg93t8/.5DQuDURa6C',	'2021-03-09 08:43:49',	'2021-03-09 08:43:49',	0);

-- 2021-03-10 17:09:28
